import { createStackNavigator, createSwitchNavigator } from 'react-navigation';
import LoginScreen from "./src/screens/login-screen/login-screen";
import HomeScreen from "./src/screens/home-screen/home-screen";

const AppStack = createStackNavigator({ 
  Home: HomeScreen
});
const AuthStack = createStackNavigator({ 
  Login: LoginScreen
});

export default createSwitchNavigator(
  {
    AppStack: AppStack,
    AuthStack: AuthStack,
  },
  {
    initialRouteName: 'AppStack',
  }
);