import React from 'react';
import styled from "styled-components";
import { View, Button, TextInput } from 'react-native';
import HourList, { apointmentType } from "../../shared/hours-list/hours-list";
import ModalComponent from "../../shared/modal/modal";
import hoursService from "../../services/hours.service";

var StyledForm = styled.View`
    width: 100%;
    height: 40px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    margin-top: 30px;
`;

const textInputStyles = {
    marginTop: 20,
    height: 50,
    fontSize: 20,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    width: 300
}

export default class HomeScreen extends React.Component {
    static navigationOptions = {
        title: 'Home'
    };

    state = {
        modalVisible: false,
        hours: []
    };

    componentDidMount(){
        this.loadHours();
    }

    loadHours() {
        hoursService.getHours().then(data => {
            console.log(data)
            this.setState({
                hours: data
            })
        });
    }

    addApointment() {
        var hour = {
            user: {
                name: "Charles",
                id: 111111
            },
            type: apointmentType.start,
            location: {
                lat: 1221,
                lng: 1232312
            }
        }

        hoursService.addHour(hour).then(() => {
            this.loadHours();
            this.setModalVisible();
        });
    }

    setModalVisible() {
        this.setState(previousState => {
            return {
                modalVisible: !previousState.modalVisible
            }
        });
    }

    render() {
        return (
            <View>
                <Button
                    onPress={() => this.setModalVisible()}
                    title="Add"
                    color="red"
                />
                <ModalComponent onCloseModalRequest={() => {
                    this.setModalVisible()
                }} modalVisible={this.state.modalVisible}>
                    <StyledForm>
                        <Button
                            onPress={() => {
                                this.addApointment();
                            }}
                            title="Learn More"
                            color="#841584"
                            accessibilityLabel="Learn more about this purple button"
                        />
                    </StyledForm>
                </ModalComponent>

                <HourList hours={this.state.hours} />
            </View>
        );
    }
}
