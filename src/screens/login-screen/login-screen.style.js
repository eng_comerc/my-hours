export const loginStyles = `
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

export const logoStyles = `
    height: 200px;
`;

export const imgStyles = `
    width: 60%;
`;

export const inputStyles = `

`;

export const textInputStyles = {
    marginTop: 20,
    height: 50,
    fontSize: 20,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    width: 300
}