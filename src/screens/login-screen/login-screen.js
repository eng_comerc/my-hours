import React from 'react';
import styled from "styled-components";
import { Image, TextInput, Button } from 'react-native';
import { loginStyles, logoStyles, imgStyles, inputStyles, textInputStyles } from "./login-screen.style";

const LoginView = styled.View`
  ${loginStyles}
`;

const LogoView = styled.View`
  ${logoStyles}
`;

const LogoImg = styled.Image`
  ${imgStyles}
`;

const InputView = styled.View`
  ${inputStyles}
`;

export default class LoginScreen extends React.Component {
    static navigationOptions = {
        header: null,
    };

    goToHoursPage(){
        this.props.navigation.navigate('AppStack')
    }

    render() {
        return (
            <LoginView>
                <LogoView>
                    <Image
                        resizeMode="contain"
                        style={{
                            flex: 1
                        }}
                        source={require('../../../assets/logo.png')}
                    />
                </LogoView>

                <InputView>
                    <TextInput
                        style={textInputStyles}
                        placeholder="Username!"
                        onSubmitEditing={() => {
                            this.passwordInput.focus();
                        }}
                        onChangeText={(username) => this.setState({username})}
                        />

                    <TextInput
                        ref={(input) => { this.passwordInput = input; }}
                        style={textInputStyles}
                        placeholder="Password!"
                        returnKeyType = { "next" }
                        onSubmitEditing={() => {
                            this.goToHoursPage();
                        }}
                        secureTextEntry={true}
                        onChangeText={(password) => this.setState({password})}
                        />

                    <Button
                        onPress={() => {
                            this.goToHoursPage();
                        }}
                        title="Learn More"
                        color="#841584"
                        accessibilityLabel="Learn more about this purple button"
                        />
                </InputView>
            </LoginView>
        );
    }
}
