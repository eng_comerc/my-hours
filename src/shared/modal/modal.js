import React from 'react';
import { View, Modal, Text, TouchableHighlight } from 'react-native';

export default class ModalComponent extends React.Component {
    render() {
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.props.modalVisible}
                onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                <View style={{ marginTop: 22 }}>
                    <View style={{
                        padding: 10
                    }}>
                        <TouchableHighlight
                            onPress={() => {
                                if(this.props.onCloseModalRequest){
                                    this.props.onCloseModalRequest();
                                }
                            }}>
                            <Text style={{
                                fontSize: 20,
                                color: "red"
                            }}>X</Text>
                        </TouchableHighlight>
                    </View>

                    {this.props.children}
                </View>
            </Modal>
        );
    }
}
