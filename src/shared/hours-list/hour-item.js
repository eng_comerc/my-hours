import React from 'react';
import styled from "styled-components";
import { View, Text, Button } from 'react-native';
import moment from "moment";

export default class HourItem extends React.Component {
    render() {
        var { item } = this.props;
        return (
            <View style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "space-around",
                flexDirection: "row",
                padding:16
            }}>
                <Text>{ moment(item.date).format("DD/MM/YYYY HH:mm") }</Text>
                <Text>{ item.type }</Text>
                <Button
                    onPress={() => {
                        
                    }}
                    title="Editar"
                    color="#841584"
                    />
            </View>
        );
    }
}
