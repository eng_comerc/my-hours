import React from 'react';
import styled from "styled-components";
import { View, FlatList } from 'react-native';
import HourItem from "./hour-item";

export const apointmentType = {
    start: "ENTRADA",
    end: "SAIDA"
}

export default class HourList extends React.Component {
    render() {
        return (
            <View>
                <FlatList
                    keyExtractor={(item, index) => item.id.toString()}
                    data={this.props.hours}
                    renderItem={({item}) => <HourItem item={item} />}
                    />
            </View>
        );
    }
}
