import Expo, { SQLite } from 'expo';
import moment from "moment";

const db = SQLite.openDatabase('db.db');

class HoursService {
    constructor() {
        db.transaction(tx => {
            tx.executeSql(
                `create table if not exists hours 
                    (
                        id integer primary key not null, 
                        type text, 
                        date text,
                        user_id integer
                    );`
            );
        });
    }

    addHour(item) {
        return new Promise((resolve, reject) => {
            try {
                db.transaction(
                    tx => {
                        tx.executeSql(`insert into hours 
                        (type, date, user_id) values (?, ?, 5)`, [
                                item.type,
                                new Date()
                            ]);
                    },
                    error => {
                        reject(error);                      
                    },
                    () => {
                        resolve(true);
                    }
                );
            } catch (error) {
                reject(error);
            }
        })

    }

    getHours() {
        return new Promise((resolve, reject) => {
            try {
                db.transaction(tx => {
                    tx.executeSql(
                        `select * from hours;`,
                        [],
                        (transaction, { rows: { _array } }) => resolve(_array)
                    );
                });
            } catch (error) {
                reject(error);
            }
        })
    }
}

var hoursService = new HoursService();
export default hoursService;